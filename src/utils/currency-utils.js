export function currencyFormat (val) {
  return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(val).replace(/,00\s€/, ' €') // Hack to not display ', 00'
}
