import { check } from 'meteor/check'
import { Meteor } from 'meteor/meteor'
import { Groups } from '../../both/groups/collection'
import { Persons } from '../../both/persons/collection'

Meteor.publish('groups', function () {
  if (!this.userId) return []
  return Groups.find()
})

Meteor.publish('groups.invite', function (id) {
  check(id, String)
  // if (!group) throw new Meteor.Error('invalid-invite', 'Invalid Group invite')
  return [
    Groups.find({ _id: id }, {
      fields: { _id: 1, name: 1 }
    }),
    Persons.find({ groups: id }, { // TODO: only persons without user
      fields: { _id: 1, name: 1 }
    })
  ]
})
