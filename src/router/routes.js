
const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      { path: '', meta: { noLoginRedirect: true }, component: () => import('pages/Index.vue') },
      { path: '/invite/:id', meta: { noLoginRedirect: true }, component: () => import('pages/Invite.vue') },
      { path: 'home', component: () => import('pages/Home.vue') },
      { path: 'expenses', component: () => import('pages/Expenses.vue') },
      { path: 'pledges', component: () => import('pages/Pledges.vue') },
      { path: 'people', component: () => import('pages/People.vue') },
      { path: 'balances', component: () => import('pages/Balances.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
