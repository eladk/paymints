import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import Meteor from 'meteor/meteor'
import moment from 'moment'
import SimpleSchema from 'simpl-schema'
import { LoggedInMixin } from '../../../utils/method-mixins'
import { Transactions } from './collection'

export const addTransaction = new ValidatedMethod({
  name: 'transactions.add',
  mixins: [LoggedInMixin, CallPromiseMixin],
  validate: new SimpleSchema({
    from: { type: String },
    to: { type: String },
    date: { type: String },
    amount: { type: Number }
  }).validator(),
  run (entry) {
    console.log(`Call to transactions.add (sim=${this.isSimulation}):`, entry)

    const dateMoment = moment(entry.date)
    if (!dateMoment.isValid()) throw new Meteor.Error('invalid-date', `Invalid moment string: ${entry.date}`, entry.date)
    entry.date = dateMoment.toDate()

    return Transactions.insert({
      ...entry,
      createdAt: new Date()
    })
  }
})
