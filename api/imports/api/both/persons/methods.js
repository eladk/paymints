import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { Meteor } from 'meteor/meteor'
import SimpleSchema from 'simpl-schema'
import { Persons } from './collection'

export const addPerson = new ValidatedMethod({
  name: 'persons.add',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    name: { type: String }
  }).validator(),
  run ({ name }) {
    console.log('Call to persons.add:', name)

    if (Persons.findOne({ name })) throw new Meteor.Error('already-exists', `Person already exists: ${name}`)

    return Persons.insert({
      name,
      createdAt: new Date(),
      createdBy: Meteor.userId()
    })
  }
})

export const addPersonsToGroups = new ValidatedMethod({
  name: 'persons.addPersonsToGroups',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    personIds: [String],
    groupIds: [String]
  }).validator(),
  run ({ personIds, groupIds }) {
    console.log('Call to persons.addPersonsToGroups:', personIds, 'to', groupIds)
    const user = Meteor.user()
    if (this.connection && (!user || !user.isAdmin)) throw new Meteor.Error('not-an-admin', 'Only admins can do this')
    if (groupIds.length > 1) throw new Meteor.Error('only-one-group', 'Multi-group is not implemented yes') // TODO: multi-group

    for (const id of personIds) {
      // const affected = Persons.update({ _id: id }, { $addToSet: { groups: { $each: groupIds } } })
      const affected = Persons.update({ _id: id }, { $set: { groups: groupIds } }) // TODO: multi-group
      if (!affected) console.warn(`addPersonsToGroups (person=${id}, groups=${groupIds}) did't affect any data`)
    }
  }
})
