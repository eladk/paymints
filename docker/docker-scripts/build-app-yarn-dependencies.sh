#!/bin/bash

set -o errexit

printf "\n[-] Installing app Yarn dependencies...\n\n"

cd $APP_SOURCE_FOLDER

meteor npx yarn install
