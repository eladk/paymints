import { Supply } from 'vue-supply'

export default {
  extends: Supply,
  // data: () => ({}),
  meteor: {
    $lazy: true
  },
  methods: {
    activate () {
      console.log('Starting supply', this)
      this.$startMeteor()
    },

    deactivate () {
      console.log('Stopping supply', this)
      this.$stopMeteor()
    },

    reloadSupply () {
      console.log('Restarting supply', this)
      this.$stopMeteor()
      this.$startMeteor()
    }
  }
}
