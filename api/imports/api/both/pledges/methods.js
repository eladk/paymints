import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import SimpleSchema from 'simpl-schema'
import { Pledges } from './collection'

export const addPledge = new ValidatedMethod({
  name: 'pledges.add',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    group: { type: String },
    title: { type: String },
    payer: { type: String },
    receiver: { type: String, required: false },
    date: { type: String },
    amount: { type: Number }
  }).validator(),
  run (entry) {
    console.log(`Call to pledges.add (sim=${this.isSimulation}):`, entry)
    // TODO: check group

    return Pledges.insert({
      ...entry,
      createdAt: new Date()
    })
  }
})
