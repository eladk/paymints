import { Meteor } from 'meteor/meteor'
import { Persons } from '../../both/persons/collection'

Meteor.publish('persons', function () {
  if (!this.userId) return []
  return Persons.find()
})
