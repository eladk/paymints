import { Meteor } from 'meteor/meteor'
import { Expenses } from '../../both/expenses/collection'

Meteor.publish('expenses', function () {
  if (!this.userId) return []
  return Expenses.find()
})
