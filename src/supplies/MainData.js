import { Expenses } from 'api/expenses/collection'
import { Groups } from 'api/groups/collection'
import { Persons } from 'api/persons/collection'
import { Pledges } from 'api/pledges/collection'
import _ from 'lodash'
import { Meteor } from 'meteor/meteor'
import { register } from 'vue-supply'
import { aggregatedBalances, aggregatedExpensesAndPledges, settlePayments } from '../utils/settle-utils'
import base from './base'

export const MainData = {
  name: 'MainData',
  extends: base,

  meteor: {
    $subscribe: {
      'Meteor.users.ownInfo': [],
      groups: [], // TODO: specific to current group?!
      persons: [],
      expenses: [],
      pledges: []
    },

    allReady () {
      console.log('allReady?', this.$subReady)
      return !_.some(this.$subReady, p => !p)
    },

    status: () => Meteor.status(),
    userId: () => Meteor.userId(),
    user () {
      console.log('MainData meteor user:', Meteor.userId(), Meteor.user())
      return Meteor.user()
    },
    allPersons () { // TODO: AdminData?
      return Persons.find({})
    },
    myPerson () {
      // if (!this.$data) return null
      if (!this.user) return null
      const person = _.first(this.user.persons)
      return person ? Persons.findOne({ _id: person }) : null
    },
    groups () {
      return Groups.find({})
    },
    myGroup () {
      // if (!this.$data) return null
      if (!this.myPerson) return null
      return Groups.findOne({ _id: { $in: this.myPerson.groups } }) // TODO: multi-group
    },
    groupMembers () {
      // if (!this.$data) return null
      if (!this.myGroup) return null
      return Persons.find({ groups: this.myGroup._id })
    },
    expenses () {
      // if (!this.$data) return null
      const group = this.myGroup
      if (!group) {
        console.log('No group!')
        return []
      }
      return Expenses.find(
        { group: group._id },
        { sort: { date: -1, createdAt: -1 } }
      ).map(p => Object.assign(p, {
        payer: _.find(this.groupMembers, { _id: p.payer }) || { invalid: true, name: p.payer }
      }))
    },
    pledges () {
      // if (!this.$data) return null
      const group = this.myGroup
      if (!group) {
        console.log('No group!')
        return []
      }
      return Pledges.find(
        { group: group._id },
        { sort: { date: -1, createdAt: -1 } }
      ).map(p => Object.assign(p, {
        payer: _.find(this.groupMembers, { _id: p.payer }) || { invalid: true, name: p.payer },
        receiver: p.receiver && (_.find(this.groupMembers, { _id: p.receiver }) || { invalid: true, name: p.receiver })
      }))
    },

    aggregatedExpensesAndPledges: function () {
      // if (!this.$data) return null
      return aggregatedExpensesAndPledges({ expenses: this.expenses, pledges: this.pledges, groups: this.groups })
    },
    aggregatedBalances: function () {
      // if (!this.$data) return null
      return aggregatedBalances(this.aggregatedExpensesAndPledges)
    },
    settlePayments: function () {
      // if (!this.$data) return null
      return settlePayments(this.aggregatedBalances)
    },

    myBalance () {
      // if (!this.$data) return null
      console.log('myBalance', this.myPerson, this.aggregatedBalances)
      if (!this.myPerson) return null
      const balances = this.aggregatedBalances
      // if (!aggregated) return 'no balances'
      return balances[this.myPerson.name]
    }
  }
}
register('MainData', MainData)
