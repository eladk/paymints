import currency from 'currency.js'
import _ from 'lodash'

const MIN_AMOUNT = 10

export function aggregatedExpensesAndPledges ({ expenses, pledges, groups }) {
  if (!expenses || !pledges) return {}
  const expensesAggregated = aggregateEntries(expenses, 'payer', groups)
  const pledgesAggregated = aggregateEntries(pledges, 'payer', groups)
  const pledgeReceives = aggregateEntries(_.filter(pledges, 'receiver'), 'receiver', groups)

  const outstanding = _.mergeWith(expensesAggregated, pledgeReceives, (a, b) => currency(a).add(b))
  const instanding = pledgesAggregated
  const persons = _.union(_.keys(outstanding), _.keys(instanding))
  const personDefaults = _.fromPairs(persons.map(e => [e, 0]))
  _.defaults(outstanding, personDefaults)
  _.defaults(instanding, personDefaults)
  const newVar = { persons, instanding, outstanding }
  console.log('aggregatedExpensesAndPledges', newVar)
  return newVar
}

export function aggregatedBalances (aggregatedExpensesAndPledges) {
  const { persons, outstanding, instanding } = aggregatedExpensesAndPledges
  if (!persons) return {}
  const balances = _.fromPairs(
    persons.map(person => {
      return [person, currency(outstanding[person]).subtract(instanding[person])]
    })
  )
  return balances
}

export function settlePayments (balances) {
  if (!balances) return null
  const settlements = settle1(balances)
    .filter(s => s.amount > MIN_AMOUNT)
  return settlements
}

function aggregateEntries (data, personField, groups) {
  if (!data.length) return {}
  console.log(`Calculating balances for ${data.length} entries`)
  const byPayer = _.groupBy(data, entry => {
    const person = entry[personField]
    if (person.settlementGroup) {
      return _.find(groups, { _id: person.settlementGroup }).name
    } else {
      return person.name
    }
  })
  return _.mapValues(byPayer, expenses => {
    return _.reduce(expenses, (sum, entry) => sum.add(entry.amount), currency(0)).value
  })
}

// basic implementation inspired from here: https://stackoverflow.com/a/52305384/1633985
export function settle1 (balances) {
  console.log('Balances:', balances)
  const people = Object.keys(balances)
  if (!people.length) return []

  const sortedPeople = people.sort((personA, personB) => balances[personA] - balances[personB])
  const sortedValuesPaid = sortedPeople.map((person) => balances[person])
  console.log('People:', sortedPeople)
  console.log('values:', sortedValuesPaid)
  // if (!sortedValuesPaid.length) return []

  let i = 0
  let j = people.length - 1
  let tries = 0
  const settlePayments = []

  console.debug(`${i},${j} START - total=`, sortedValuesPaid.reduce((curr, val) => curr.add(val)).format())
  while (i < j) {
    const maxWhatShouldBeReceived = currency(sortedValuesPaid[i]).multiply(-1)
    const debt = maxWhatShouldBeReceived.value < sortedValuesPaid[j].value ? maxWhatShouldBeReceived : sortedValuesPaid[j]
    if (debt.value < 0) {
      console.debug(`Negative total (${sortedValuesPaid[i]}, ${sortedValuesPaid[j]}) - remaining owers: `, _.fromPairs(sortedValuesPaid.map((v, i) => v.value !== 0 ? [sortedPeople[i], v.format()] : null).filter(v => v !== null)))
      break
    }
    const before = `${i},${j} : (before) ${sortedValuesPaid[i]} , ${sortedValuesPaid[j]}`
    sortedValuesPaid[i] = sortedValuesPaid[i].add(debt)
    sortedValuesPaid[j] = sortedValuesPaid[j].subtract(debt)

    settlePayments.push({ from: sortedPeople[i], to: sortedPeople[j], amount: debt })
    console.groupCollapsed(`${i},${j} - ${sortedPeople[i]} owes ${sortedPeople[j]}`, debt.format())
    console.debug(before)
    console.debug(`${i},${j} : ${sortedValuesPaid[i]} , ${sortedValuesPaid[j]}`)
    console.groupEnd()

    if (sortedValuesPaid[i].value === 0) {
      i++
    }
    if (sortedValuesPaid[j].value === 0) {
      j--
    }
    tries++
    if (tries > 100) throw new Error(`Tried 100 iterations - no luck: ${JSON.stringify(sortedValuesPaid)}`)
  }

  console.debug('values after:', sortedValuesPaid)
  return settlePayments
}
