import { Meteor } from 'meteor/meteor'

Meteor.publish('Meteor.users.ownInfo', function () {
  // from: https://guide.meteor.com/accounts.html#publish-custom-data
  if (!this.userId) return [] // throw new Meteor.Error('not-logged-in', 'No userId')

  return Meteor.users.find({ _id: this.userId }, {
    fields: { isAdmin: 1, persons: 1, readOnly: 1 }
  })
})
